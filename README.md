php-rest-client
===============

A PHP client for REST calls.

Integrates Basic and Digest HTTP Authentication.
