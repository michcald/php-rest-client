<?php

include '../../Rest/RestClient.php';

$baseUrl = 'http://localhost/development/rest-client/samples/server';

$client = new RestClient($baseUrl);

$client->setBasicAuth('michael', 'ciao');

echo $client->get('index.php', array(
    'name' => 'John'
));