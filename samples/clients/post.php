<?php

include '../../Rest/RestClient.php';

$url = 'http://localhost/development/rest-client/samples/server/index.php';

$post = new Rest_Request_Method_Post($url);

$post->addParam('nome', 'ciao');
$post->addParam('file1', '@files/readme.txt');
$post->addParam('file2', '@files/img.jpg');

echo $post->execute();