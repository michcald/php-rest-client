<h1>Rest Server</h1>
<style tyle="text/css">
    th {
        text-align: left;
        vertical-align: top;
    }
    td, th {
        padding: 5px;
    }
</style>
<table>
    <tr>
        <th>Host:</th>
        <td><?=$_SERVER['HTTP_HOST']?></td>
    </tr>
    <tr>
        <th>Method:</th>
        <td><?=$_SERVER['REQUEST_METHOD']?></td>
    </tr>
    <tr>
        <th>Time:</th>
        <td><?=date('d/m/Y, h:i:s', $_SERVER['REQUEST_TIME'])?></td>
    </tr>
    <tr>
        <th>Basic Authentication:</th>
        <td>Username(<?=$_SERVER['PHP_AUTH_USER']?>), Password(<?=$_SERVER['PHP_AUTH_PW']?>)</td>
    </tr>
    <tr>
        <th>Get Params:</th>
        <td><pre><?=print_r($_GET, true)?></pre></td>
    </tr>
    <tr>
        <th>Post Params:</th>
        <td><pre><?=print_r($_POST, true)?></pre></td>
    </tr>
    <?parse_str(file_get_contents("php://input"), $input)?>
    <tr>
        <th>Put Params:</th>
        <td><pre><?=print_r($input, true)?></pre></td>
    </tr>
    <tr>
        <th>Delete Params:</th>
        <td><pre><?=print_r($input, true)?></pre></td>
    </tr>
    <tr>
        <th>Files:</th>
        <td><pre><?=print_r($_FILES, true)?></pre></td>
    </tr>
</table>