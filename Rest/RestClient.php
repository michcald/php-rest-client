<?php

// Version 1.0
// Michael Caldera

include 'Request/Curl.php';
include 'Request/Method/Abstract.php';
include 'Request/Method/Get.php';
include 'Request/Method/Post.php';
include 'Request/Method/Put.php';
include 'Request/Method/Delete.php';
include 'Request/Auth/Abstract.php';
include 'Request/Auth/None.php';
include 'Request/Auth/Basic.php';
include 'Response.php';
include 'Exception/IllegalRestMethodException.php';

class RestClient {

    private $baseUrl = null;

    /**
     *
     * @var Rest_Request_Auth_Abstract
     */
    private $auth = null;

    public final function __construct($baseUrl) {
        $this->baseUrl = $baseUrl;
    }

    public final function setBasicAuth($username, $password) {
        $this->auth = new Rest_Request_Auth_Basic($username, $password);
    }

    public final function get($resource, $params = array()) {
        return $this->call('get', $resource, $params);
    }

    public final function post($resource, $params = array()) {
        return $this->call('post', $resource, $params);
    }

    public final function put($resource, $params = array()) {
        return $this->call('put', $resource, $params);
    }

    public final function delete($resource, $params = array()) {
        return $this->call('delete', $resource, $params);
    }

    private final function call($method, $resource, $params = array()) {
        $url =  $this->baseUrl . '/' . $resource;

        $call = null;

        switch ($method) {
            case 'get':
                $call = new Rest_Request_Method_Get($url);
                break;
            case 'post':
                $call = new Rest_Request_Method_Post($url);
                break;
            case 'put':
                $call = new Rest_Request_Method_Put($url);
                break;
            case 'delete':
                $call = new Rest_Request_Method_Delete($url);
                break;
            default:
                throw new IllegalRestMethodException("Method '$method' not valid");
        }

        if($this->auth != null) {
            $call->setAuth($this->auth);
        }

        foreach($params as $key => $value) {
            if($value) {
                $call->addParam($key, $value);
            }
        }
        return $call->execute()->getBody();
    }
}