<?php

class Rest_Response {

    private $contentType = '';

    private $body = '';

    public final function __construct() {

    }

    public final function setContentType($contentType) {
        $this->contentType = $contentType;
    }

    public final function setBody($body) {
        $this->body = is_string($body) ? $body : '';
    }

    public final function getBody() {
        return $this->body;
    }

    public final function __toString() {
        return $this->getBody();
    }
}