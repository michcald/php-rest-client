<?php

class Rest_Request_Method_Delete extends Rest_Request_Method_Abstract {

    public final function execute() {
        $url = $this->getUrl();

        $curl = new Rest_Request_Curl();
        $curl->setOption(CURLOPT_TIMEOUT, 30)
                ->setOption(CURLOPT_URL, $url)
                ->setOption(CURLOPT_RETURNTRANSFER, true)
                ->setOption(CURLOPT_HTTPHEADER, array(
                    'Accept: ' . $this->getResponseType()
                ))
                ->setOption(CURLOPT_POSTFIELDS, http_build_query($this->getParams()))
                ->setOption(CURLOPT_CUSTOMREQUEST, 'DELETE');

        $this->getAuth()->set($curl);

        $result = $curl->execute();

        $response = new Rest_Response;
        $response->setBody($result);
        return $response;
    }
}