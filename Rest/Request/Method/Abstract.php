<?php

abstract class Rest_Request_Method_Abstract {

    private $url = '';

    private $params = array();

    private $responseType = 'application/json';

    /**
     *
     * @var Rest_Response_Auth_Abstract
     */
    private $auth = null;

    public final function __construct($url) {
        $this->url = $url;
        $this->auth = new Rest_Request_Auth_None();
    }

    public final function setAuth(Rest_Request_Auth_Abstract $auth) {
        $this->auth = $auth;
    }

    /**
     *
     * @return Rest_Request_Auth_Abstract
     */
    protected final function getAuth() {
        return $this->auth;
    }

    public final function addParam($key, $value) {
        $this->params[$key] = $value;
    }

    public final function setResponseType($contentType) {
        $this->responseType = $contentType;
    }

    protected final function getResponseType() {
        return $this->responseType;
    }

    protected final function getUrl() {
        return $this->url;
    }

    protected final function getParams() {
        return $this->params;
    }

    /**
     * @return Rest_Response
     */
    abstract public function execute();
}