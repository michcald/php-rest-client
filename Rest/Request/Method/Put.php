<?php

class Rest_Request_Method_Put extends Rest_Request_Method_Abstract {

    public final function execute() {
        $url = $this->getUrl();

        $request = http_build_query($this->getParams());

        $putData = tmpfile();
        fwrite($putData, $request);
        fseek($putData, 0);

        $curl = new Rest_Request_Curl();
        $curl->setOption(CURLOPT_TIMEOUT, 30)
                ->setOption(CURLOPT_URL, $url)
                ->setOption(CURLOPT_RETURNTRANSFER, true)
                ->setOption(CURLOPT_HTTPHEADER, array(
                    'Accept: ' . $this->getResponseType() // What kind of content we'll accept as a response
                ))
                ->setOption(CURLOPT_PUT, true)
                ->setOption(CURLOPT_INFILE, $putData)
                ->setOption(CURLOPT_INFILESIZE, strlen($request));

        $this->getAuth()->set($curl);

        $result = $curl->execute();

        fclose($putData);

        $response = new Rest_Response();
        $response->setBody($result);
        return $response;
    }
}