<?php

class Rest_Request_Curl {

    private $ch = null;

    public final function __construct() {
        $this->ch = curl_init();
    }

    public final function setOption($option, $value) {
        curl_setopt($this->ch, $option, $value);
        return $this;
    }

    public final function execute() {
        return curl_exec($this->ch);
    }

    public final function __destruct() {
        curl_close($this->ch);
    }
}