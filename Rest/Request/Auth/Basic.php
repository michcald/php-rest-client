<?php

class Rest_Request_Auth_Basic extends Rest_Request_Auth_Abstract {

    private $username;

    private $password;

    public function __construct($username, $password) {
        $this->username = $username;
        $this->password = $password;
    }

    public final function set(Rest_Request_Curl &$curl) {
        $pwd = $this->username . ':' . $this->password;
        $curl->setOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC)
                ->setOption(CURLOPT_USERPWD, $pwd);
    }

}